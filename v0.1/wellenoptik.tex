\chapter{Wellenoptik}
\index{Wellenoptik}
Im Gegensatz zu der geometrischen Optik (siehe Abschnitt
\ref{section:geometrischeOptik}) berücksichtigen wir in diesem Abschnitt die
Welleneigenschaften des Lichtes. Durch die Welleneigenschaften kommt es zu 
zunächst überraschenden Phänomenen wie Beugung und Interferenz. 

\section{Huyghen'sches Prinzip}
Das \index{Huyghen'sche Prinzip} Huyghen'sche Prinzip erlaubt uns, 
eine jede Welle als Überlagerung von einzelnen Kugelwellen (sogenannten Elementarwellen \index{Elementarwellen}) zu verstehen. 

Mit Hilfe des Huyghen'schen Prinzips lassen sich Phänomene erklären, die im
Rahmen der geometrischen Optik nicht erwarten werden. Ein einfaches Beispiel
ist der Schattenwurf (siehe Abbildung \ref{figure:huygens}): In der Näherung
der geometrischen Optik erzeugt ein Hindernis (z.B. eine Schneide) einen scharf
umrissenen Schatten: kein Strahl eines parallel einfallenden Bündels erreicht
die abgeschattete Region. Tatsächlich ist auf der Größenskala der Wellenlänge
des einfallenden Lichtes zu beobachten, dass Licht auch in die abgeschattete
Region dringen kann. 
%
\begin{figure}
	\includegraphics[width=0.7\linewidth]{pictures/drawing_huygens.png}
	%
	\caption{\label{figure:huygens} 
	Skizze oben: Im Grenzfall der
	geometrischen Optik entsteht hinter einem Hindernis/Blende ein scharfer
	Schatten. Skizze unten: In der Region um das Hindernis (Größenordnung
	der Wellenlänge) konstruieren wir die ebenen Wellenfronten aus der
	Überlagerung von einzelnen Elementarwellen. Mit dieser Konstruktion
	wird klar, dass der Schatten (auf der Größenordnung von
	Wellenlängen) teilweise ausgeleuchtet ist.} 
\end{figure}
\textit{Experiment zum Nachdenken: Schauen Sie sich an einem sonnigen Tag den Schattenwurf Ihrer Hand an. Bringen Sie Daumen und 
Zeigefinger zusammen. Was beobachten Sie im Schattenwurf kurz bevor sich Daumen und Zeigefinger berühren?}
%%%
\section{Koh\"arenz und Interferenz}
\index{Kohärenz} \index{Interferenz}
%
Eine Welle ist \textit{kohärent} 
\begin{itemize}
		 \item während der \textit{Kohärenzzeit} $t_c$: an einem festen Ort ändert sich die Phase der Welle während einer Periode 
			 um $2\pi$;
		 \item über eine \textit{Kohärenzlänge} $\ell_c$: die Amplituden haben zu allen Zeiten zwischen zwei Punkten innerhalb der Kohärenzlänge einen zeitlich konstanten 
			 Phasenunterschied.
\end{itemize}
Es gilt für eine Welle mit Ausbreitungsgeschwindigkeit $\ell_c = c t_c$.  Eine ideale ebene Welle hat $t_c, \ell_c \rightarrow \infty$.


Aufgrund der linearen Eigenschaft der Wellengleichung lassen sich mehrere Wellen addieren.
	Für den Spezialfall zweier Wellen \textit{gleicher}
	Amplitude, \textit{gleicher} Frequenz und gleicher Polarisation ergibt
	sich je nach \textit{relativer} Phase $\Delta \phi$ eine
	\textbf{konstruktive Interferenz} für $\Delta \phi=0,2\pi,4\pi,\ldots ,
	m\cdot 2\pi$ und eine \textbf{destruktive
	Interferenz}\index{Interferenz} für $\Delta
	\phi=\pi,3\pi,5\pi,\ldots,\frac{2m+1}{2}\pi$. 

Wir nehmen zwei Wellen an mit $E_1 = a\sin(\omega t)$ und $E_2 = a\sin(\omega t + \Delta\varphi)$.
Für die Summe 
$$ E = a \sin(\omega t)(1+\cos\Delta\varphi) + a\cos(\omega t)\sin\Delta\varphi.$$
Die \textit{zeitlich gemittelte} quadratische Amplitude ergibt sich zu
\[
	\langle E^2\rangle = \frac{a^2}{2}( (1+\cos\Delta\varphi)^2 +  \sin^2\Delta\varphi)=a^2(1+\cos\Delta\varphi) = (\langle E_1^2\rangle + \langle E_2\rangle^2)(1+\cos\Delta\varphi) .
\]
Für eine vollständig kohärente Überlagerung kann es mit $\Delta \varphi=0,\pm 2\pi,\ldots$ zu \textit{konstruktiver} Interferenz kommen. Bei einem Phasenunterschied
der beiden Wellen um $\Delta\varphi=\pm \pi/2, \pm 3\pi/2,\ldots$ kommt es zu \textit{destruktiver} Interferenz mit vollständiger Auslöschung der beiden Wellen!

Für nicht-kohärente Wellen kommt es zu keiner Interferenz. Bei gleichförmiger Verteilung des Phasenunterschieds $\Delta \phi$ verschwindet bei der zeitlichen Mittelung der
Term $\langle \cos\Delta\varphi\rangle=0$. 


\paragraph{Linienbreite und Kohärenz}
 Wir betrachten  Wellen der Frequenz $\omega$, die aus einer punktförmigen
 Quelle stammen, sich aber um $\Delta\omega\ll\omega$ in der Frequenz
 unterscheiden (Linienbreite), so dass sich Wellen mit Frequenzen im Bereich
 von etwa $\omega_0-\Delta \omega$ bis zu $\omega_0+\Delta \omega$  überlagern.
 Wenn wir jetzt an einem festen Ort $\bm{r}=\bm{r}_p$ zwei Wellen betrachten,
 ergibt sich für den zeitlichen Verlauf der Amplituden das
 Bild~\ref{fig:coherence_time}: Der Phasenunterschied steigt mit zunehmender
 Zeit an, bis für $t=t_c=2\pi/\Delta \omega$ der Phasenunterschied $\Delta
 \phi=2\pi$ ist.

\textit{Beispiele:} 
\begin{itemize}
 \item Sichtbares Licht: Hier ist $\Delta \nu\approx 10^{14}~$Hz, entsprechend ist $\Delta s_c\approx3~\mu$m,
  \item isoliertes Atom: $\Delta \nu \approx 10^{8}$~Hz, so dass $\Delta s_c\approx 3~$m,
  \item Laser: $\Delta \nu \approx $kHz für sehr gut stabilisierte Laser, damit ist $s_c\approx 300$~km. 
\end{itemize}	

Wenn wir die Interferenz von elektromagnetischen Wellen untersuchen, ist die
Kohärenzlänge einer Quelle eine wichtige Größe. Wenn wir z.B. mit einem
Strahlteiler das Licht aufteilen und über einen zweiten Strahlteiler wieder
zusammenführen, können wir prinzipiell konstruktive oder auch destruktive
Interferenz beobachten, wenn sich die Phasen der beiden Wellen mit einem
Phasenunterschied $\Delta \phi = 0,2\pi,4\pi,\ldots$ (konstruktiv) bzw. $\Delta
\phi = \pi, 3\pi, 5\pi\,..$ (destruktiv) überlagern. Ist jedoch der
Wegunterschied zwischen den beiden Lichtwegen größer als die Kohärenzlänge, ist
kein Interferenzeffekt mehr beobachtbar. Umgekehrt lässt sich mit einem solchen
Verfahren die Kohärenzlänge und damit die Linienbreite der Quelle untersuchen.



\begin{figure}
	\centerline{
	\includegraphics[width=0.8\linewidth]{figures/coherence.png}}
	\caption{\label{fig:coherence_time}
	Der zeitliche Verlauf der ortsfesten Amplituden von zwei Wellen mit leicht unterschiedlicher Frequenz. Gezeigt ist der Verlauf bis
	$t_c/2=\pi/\Delta \omega$ }
\end{figure}
\section{Interferenz von zwei Wellen}
Um die Interferenz von zwei Wellen nachzuweisen, benötigen wir mindestens zwei Quellen von kohärenten Wellen. Wir nehmen  zwei punktförmige Quellen 
im Abstand $d$ zueinander  an mit folgenden Eigenschaften:
\begin{enumerate}[i)]
	\item gleiche Amplitude,
	\item gleiche Phase (bzw. zeitlich konstanter Phasenunterschied),
	\item gleiche Frequenz,
	\item gleiche Polarisation.
\end{enumerate}
Wenn wir uns das resultierende Wellenfeld (z.B. für zylinderförmige Wellen) zu einem festen Zeitpunkt nach Einschalten der beiden Quellen anschauen, erhalten wir
die Anordnung in Abb.~\ref{nodal_antinodal}.
Für einen Punkt $P$ im Abstand $r_1$ und $r_2$ zu den beiden Quellen ergibt sich folgende Bedingung für \textit{konstruktive Interferenz}:
$$ r_2 - r_1 = 0, \pm \lambda, \pm 2\lambda, \ldots, \pm n\lambda.$$
Für \textit{destruktive} Interferenz ist die Bedingung
$$ r_2 - r_1 = \pm \frac{\lambda}{2}, \pm \frac{3\lambda}{2}, \ldots, \frac{2n+1}{2}\lambda.$$
 Die beiden Gleichungen charakterisieren Hyperbeln, sogenannte Nodal-, bzw. Antinodallinien, entlang derer sich die Wellen konstruktiv bzw. destruktiv überlagern. 
Das Wellenfeld lässt sich anschaulich mit Oberflächenwellen in einem Wellenbecken darstellen.

Insgesamt finden wir $2d/\lambda$ Nodal/Antinodallinien für den gegebenen Spaltabstand $d$ bei einer Wellenlänge $\lambda$. 

Als nächstes schauen wir uns die Intensitätsverteilung der beiden Wellen an einer
Projektionswand im Abstand $L\gg d$ zu den Quellen an. 
Die hyperbolischen Nodallinien schmiegen sich an Geraden an, die einen Winkel $\theta$ zur Horizontalen haben (siehe Abbildung~\ref{nodal_antinodal}).

Wir können jetzt die Winkelbedingung für konstruktive 
$$ r_1 - r_2 = d \sin\theta = n\lambda$$
und destruktive Interferenz 
$$ r_1 - r_2 = d\sin \theta = \frac{2n+1}{2}\lambda$$
aufschreiben. 
Für große Abstände der Quellen zum Schirm nähern wir die Hyperbeln durch Geraden mit kleinen Winkeln
$\theta\ll 1$ an, 
so dass für die Positionen der Maxima auf dem Schirm im Abstand $L\gg d$ gilt:
$$  x_i = \theta_i \cdot L = n \frac{\lambda}{d} L,$$
für $n=0, \pm 1, \pm 2, \ldots $.  Die Position $x_i$ ist jeweils der Abstand
zu dem Maximum für $n=0$. 
Die Minima finden sich entsprechend bei
$$ x_i = n \frac{2n+1}{2} \frac{\lambda}{d} L.$$

Ein einfaches Verfahren zur Erzeugung von zwei kohärenten Quellen ist der sogenannte \textit{Fresnel}-Doppelspiegel. Hierbei werden zwei Spiegel um einen kleinen Winkel zueinander verkippt. Wenn wir jetzt mit einer Lichtquelle die beiden Spiegel ausleuchten, erzeugen die beiden Spiegel jeweils eine optische Abbildung der einfallenden Welle, die dann zur Überlagerung der beiden kohärenten Wellen führt (siehe Abbildung~\ref{fig_fresnel}).

Die dabei auf dem Schirm im Abstand $s=R+d$ entstehenden Streifen haben  den Abstand $\Delta x\approx \lambda s/a$.



\begin{figure}
\includegraphics[width=\linewidth]{Graphiken/young_doppelspalt.png}
	\caption{Zwei punktförmige Quellen von Wellen gleicher Wellenlänge, Amplitude und Phase erzeugen zwei sich überlagernde Wellen. Die einzelnen Wellen haben Maxima (durchgezogene Linien) und Minima (gestrichelte Linien). Es existieren Punkte, an denen sich Maxima und Minima jeweils aufaddieren. Diese Punkte sind entlang der \textit{Nodallinien} (in rot) angeordnet. Entlang der Nodallinien kommt es zu \textit{konstruktiver} Interferenz. Es existieren auch Punkte, an denen sich Maxima und Minima addieren. Diese \textit{destruktive Interferenz} findet entlang der \textit{Antinodallinie} (in blau) statt.\label{nodal_antinodal}}
\end{figure}

\begin{figure}
	\includegraphics[width=\linewidth]{double_slit.pdf}
	\caption{Bei großen Abständen verlaufen die Nodallinien entlang der Geraden mit Winkel $\theta$ zur Horizontalen.\label{fig:asymptotic_nodal}}
\end{figure}

\begin{figure}
	\centerline{	\includegraphics[width=0.7\linewidth]{fresnel.png}}
	\caption{Fresnel'scher Doppelspiegel zur Erzeugung von Interferenzmustern: Die Lichtquelle $S$ leuchtet zwei leicht zueinander verkippte Spiegel aus, so dass
	es zur Überlagerung von Wellen kommt, die scheinbar aus den Punkten $S_1$ und $S_2$ im Abstand $d$ zueinander stammen (Abbildung aus E. Hecht: Optik).
	\label{fig_fresnel}}
\end{figure}

\section{Beugung an einem Spalt}
\label{section:resolution}
\label{section:diffraction}
\section{Beugung an einem Doppelspalt}
\section{Beugung am regelmäßigen Gitter}
\section{Interferenz an d\"unnen Schichten}
\begin{figure}
	\centerline{
		\includegraphics[width=0.7\linewidth]{Graphiken/thinlayer_slanted.png} }
      	\caption{Interferenz an dünner Schicht der Dicke $d$: 
	Der einfallende Lichtstrahl wird an den beiden Grenzflächen jeweils gebrochen und reflektiert. 
	Der Wegunterschied der beiden Strahlen  
	$\Delta L=\overline{AB}+\overline{BC}-\overline{AD}=2d\sqrt{n_2-n_1\sin^2\alpha}$ 
	führt zu einem Phasenunterschied $\Delta \phi=n_2 \Delta L/\lambda + \Phi_{23} + \Phi_{21}-\Phi_{12}$, 
	wobei $\Phi_{ij}$ den möglichen Phasensprung beim Übergang bzw. Reflexion an der Grenzschicht von dem Medium mit $n_i$ und $n_j$ angibt. }    
\end{figure}
\section{Interferometer}
