from matplotlib import pyplot as plt
import numpy as np

plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
plt.xticks([])
plt.yticks([])
ax.set_ylim([-1.5,1.5 ])

x=np.linspace(0.,3.*np.pi,300)
y=np.cos(x)
y2=0.7*np.cos(x-np.pi/2.)
y1=np.zeros(300)

#plt.annotate(
#    'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#    xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

plt.title("Scope view: $S_L$ closed")
plt.plot(y,label="U(t)")
plt.plot(y1)
plt.plot(y2,label="I(t)")
plt.savefig('sl_closed.png',bbox_inches='tight')
plt.legend()
plt.show()

