from matplotlib import pyplot as plt
import numpy as np

plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
#plt.yticks([])
ax.set_ylim([0,1.2])
ax.set_xlim([0,5.])
ax.set_xlabel("t [s]")
ax.set_ylabel("U_C/U_C(t=0)")

x=np.linspace(0.,5,100)
print x
y =1.-np.exp(-x)
y2=1.-np.exp(-0.1*x)
y3=1.-np.exp(-10.*x)

#plt.annotate(
#    'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#    xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

plt.title("Spannung im RC-Kreis: Einschaltvorgang")
plt.plot(x,y,label="RC=1 s")
plt.plot(x,y2,label="RC=10 s")
plt.plot(x,y3,label="RC=0.1 s")
plt.legend()
plt.savefig('rc_voltage.png',bbox_inches='tight')

plt.show()

