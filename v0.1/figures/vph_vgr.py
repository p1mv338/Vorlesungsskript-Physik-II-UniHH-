from matplotlib import pyplot as plt
import numpy as np

#plt.xkcd()

fig = plt.figure()
plt.rc('text', usetex=True)
ax = fig.add_subplot(1, 1, 1)
#plt.rc('font',family='serif')
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
#plt.yticks([])
ax.set_ylim([0,5.])
ax.set_xlim([0,5.])
ax.set_xlabel(r'\omega/\omega_\mathrm{g}')
ax.set_ylabel(r'v/c')

x0=np.linspace(0,1.,3)
yt=np.ones(x0.size)*5.
yb=np.zeros(x0.size)
x=np.linspace(1.0001,5,100)
y =1./np.sqrt(1.-1./x**2)
y1 =np.sqrt(1.-1./x**2)

#plt.annotate(
#    'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#    xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

#plt.title(" im LC-Kreis: Einschaltvorgang")
plt.plot(x,y1,label='Gruppengeschwindigkeit')
plt.plot(x,y,label='Phasengeschwindigkeit')
plt.fill_between(x0,yb,yt,facecolor='red',alpha=0.2)
plt.plot(x,np.ones(x.size),linestyle='--')
plt.legend()
plt.savefig('vph_gr.png',bbox_inches='tight')
plt.show()
