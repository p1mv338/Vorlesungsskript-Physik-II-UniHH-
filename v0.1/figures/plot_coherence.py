from matplotlib import pyplot as plt
import numpy as np

plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
plt.xticks([])
plt.yticks([])
ax.set_ylim([-1.5,1.5 ])

x=np.linspace(0.,12.*np.pi,300)
y=np.sin(x)
y2=np.sin(x*1.1)
y1=np.zeros(300)

#plt.annotate(
#    'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#    xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

plt.annotate('$t_c/2$',horizontalalignment='center',xy=(10./12.*300,1.5),
        arrowprops=dict(arrowstyle='-'),xytext=(10./12.*300.,-1.8)) 
plt.title("Coherence time")
plt.plot(y,label="$\omega_0$")
plt.plot(y1)
plt.plot(y2,label="$\omega_0+\Delta\omega$")
plt.legend(loc='lower left')
plt.savefig('coherence.png',bbox_inches='tight')

plt.show()

