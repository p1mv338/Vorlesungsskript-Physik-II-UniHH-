from matplotlib import pyplot as plt
import numpy as np
plt.rc('text', usetex=True)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim([1,10])
ax.set_ylim([0.94,1.04])
ax.set_xticklabels([])
ax.set_yticklabels([])
plt.tick_params(axis='x',which='both',bottom='off',top='off')
plt.tick_params(axis='y',which='both',left='off',right='off')


w0=5.0
g0=1.4
x=np.linspace(1,10,600)
plt.plot(x,1+0.2*(w0**2-x**2)/((w0**2-x**2)**2+g0**2*x**2),label="n'")
plt.plot(x,0.94+2.*x/10.*0.2*g0*w0/((w0**2-x**2)**2+g0**2*x**2),label=r'\alpha')
plt.axvline(w0-0.5*g0,linestyle=':')
plt.axvline(w0+0.5*g0,linestyle=':')
plt.axhline(1,linestyle=':')
plt.title('Anomale Dispersion',color='red')
plt.fill_between([w0-0.5*g0,w0+0.5*g0],[0.9,0.9],[1.04,1.04],facecolor='red',alpha=0.2)
plt.text(w0-0.5*g0,0.935,r'\omega_0-\frac{\gamma}{2}',horizontalalignment='center')
plt.text(w0+0.5*g0,0.935,r'\omega_0+\frac{\gamma}{2}',horizontalalignment='center')
#plt.axvline(xt2)
plt.legend()
plt.savefig('plot_nprime.png',bbox_inches='tight')
plt.show()
