import numpy as np
import matplotlib.pyplot as plt

#plt.xkcd()
r=np.arange(0,2,0.001)
theta=2.*np.pi*r
r=(np.sin(np.pi/2.-theta))**2
ax=plt.subplot(111,projection='polar')
ax.plot(theta,r,label="v=0")
beta=0.3
gam=np.sqrt(1./(1.-beta*beta))
r=1./(1.-beta*np.cos(theta))**3*(1.-np.sin(theta)**2/gam**2/(1.-beta*np.cos(theta))**2)
ax.plot(theta,r,label="v=0.3c")
beta=0.4
r=1./(1.-beta*np.cos(theta))**3*(1.-np.sin(theta)**2/gam**2/(1.-beta*np.cos(theta))**2)
ax.plot(theta,r,label="v=0.4c")
ax.set_rmax(3)
#ax.set_rticks([0.5,1])
ax.set_rlabel_position(-22.5)
ax.legend()
plt.savefig("polar_2.png",bbox_inches="tight")
plt.show()
