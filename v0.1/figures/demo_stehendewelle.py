from matplotlib import pyplot as plt
import numpy as np

plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
#plt.xticks([])
#plt.yticks([])
ax.set_ylim([-2.1,2.1 ])
ax.set_xlabel("Wellenl.")
ax.set_ylabel("Amplitude")

x=np.linspace(0.,3.,300)
k=2.*np.pi
g=np.sin(k*x)
plt.plot(x,g*2.,label="t=0,T,2T,..")
plt.plot(x,g*0.,label="t=T/4,3T/4,5T/4,..")
plt.plot(x,g*(-2.),label="t=T/2,3T/2,5T/2,..")

#plt.annotate(
#    'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#    xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

plt.legend()
plt.savefig('stehendewelle.png',bbox_inches='tight')
plt.show()

