import numpy as np
import matplotlib.pyplot as plt

#plt.xkcd()
r=np.arange(0,1,0.001)
theta=2.*np.pi*r
r=(np.sin(theta))**2
ax=plt.subplot(111,projection='polar')
ax.plot(theta[r>0],r[r>0],label="v=0")
beta=0.3
r=np.sin(theta)**2/np.cos(theta)*(1./(1.-beta*np.cos(theta))**4-1.)
ax.plot(theta[r>0],r[r>0],label="v=0.3c")
beta=0.4
r=np.sin(theta)**2/np.cos(theta)*(1./(1.-beta*np.cos(theta))**4-1.)
ax.plot(theta[r>0],r[r>0],label="v=0.4c")
ax.set_rmax(3)
#ax.set_rticks([0.5,1])
ax.set_rlabel_position(-22.5)
ax.legend()
plt.savefig("polar_1.png",bbox_inches="tight")
plt.show()
