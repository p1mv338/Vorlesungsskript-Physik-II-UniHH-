from matplotlib import pyplot as plt
import numpy as np
fig = plt.figure()
#plt.rc('text', usetex=True)
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim([0,10*np.pi])
ax.set_ylim([-1.5,1.5])
ax.set_xticklabels([])
ax.set_yticklabels([])
plt.tick_params(axis='x',which='both',bottom='off',top='off')
plt.tick_params(axis='y',which='both',left='off',right='off')

e0=1.0
e1=e0
xt1=3.2*np.pi
xt2=7.0*np.pi
k1=1.
k2=k1*1.5
x=np.linspace(0,10.*np.pi,600)
x0=np.linspace(0,xt1,200)
x1=np.linspace(xt1,xt2,200)
x2=np.linspace(xt2,10.*np.pi,200)
y0=e0*np.sin(k1*x0)
y1=e1*np.sin(k2*(x1-xt1)+k1*xt1)
y2=e0*np.sin(k1*(x2-xt2)+k2*(xt2-xt1)+k1*xt1)
plt.plot(x0,y0,'b')
plt.plot(x1,y1,'g')
plt.plot(x2,y2,'b')
plt.plot(x,e0*np.sin(k1*x),'b:')
plt.text(0.5*(xt1+xt2),1.2,'n=1.5',horizontalalignment='center')
plt.text(0.5*xt1,1.2,'n=1',horizontalalignment='center')
plt.text(0.5*(xt2+10.*np.pi),1.2,'n=1',horizontalalignment='center')
plt.axvline(xt1)
plt.axvline(xt2)
plt.savefig('plt_wave.png',bbox_inches='tight')
plt.show()
