import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim([0.,90. ])
ax.set_ylim([0.,1. ])
ax.set_xlabel('$\\alpha$')
ax.set_title('$n_1=4/3$, $n_2=1$')

alph=np.linspace(0.01,90,200)
n1=4./3.
n2=1.0

alpmax=np.arcsin(n2/n1)
alp  = np.minimum(alpmax,alph * np.pi/180.)  # in radiant



sina = np.sin(alp)
sinb = n1*sina/n2

bet  = np.minimum(np.pi/2.,np.arcsin(sinb))  # in radiant


rs = - np.sin(alp-bet)/np.sin(alp+bet)
rp =   np.tan(alp-bet)/np.tan(alp+bet)

ts = np.sin(2.*alp)*np.sin(2.*bet)/(np.sin(alp+bet))**2
tp = np.sin(2.*alp)*np.sin(2.*bet)/(np.sin(alp+bet))**2/(np.cos(alp-bet))**2


plt.plot(alph,(rs*rs),label="Refl. $s$-Polarization",color='green',linestyle='solid')
plt.plot(alph,(rp*rp),label="Refl. $p$-Polarization",color='blue',linestyle='solid')


plt.plot(alph,(ts),label="Trans. $s$-Polarization",color='green',linestyle='dashed')
plt.plot(alph,(tp),label="Trans. $p$-Polarization",color='blue',linestyle='dashed')
plt.legend()
plt.savefig('plot_reftrans.png',bbox_inches='tight')
plt.show()


