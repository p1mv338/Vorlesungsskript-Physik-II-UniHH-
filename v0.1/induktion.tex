\chapter{Elektrodynamik}
 In den vorangegangenen Abschnitten haben wir angenommen, dass die 
 Ladungsverteilungen und Ströme stationär sind ($\dot{\rho}=0$,
$\dot{\mathbf{j}}=0$).
\footnote{Überlegen Sie sich, warum die zeitliche Ableitung der 
 Ladungsdichte im Falle von stationären Strömen verschwindet.} 
 
 Bei stationären Ladungen und Strömen sind die resultierenden elektrischen
 und magnetischen Felder ebenfalls zeitlich konstant (Elektrostatik
 und Magnetostatik). 


Wir betrachten im Folgenden den Fall eines sich zeitlich ändernden magnetischen
Flusses, der zur \textit{Induktion eines nicht-konservativen elektrischen
Feldes} führt. 

 \section{Faraday'sches Induktionsgesetz}
 \label{section:faraday}


  Das physikalische Prinzip des Faraday'schen Induktionsgesetzes ist die
  wesentliche Grundlage der modernen auf Strom basierenden Energiewirtschaft 
  \footnote{Ausnahmen sind z.B. Photovoltaik und Brennstoffzellen}.

Differenzialform:
  \begin{equation}
    \label{eqn:faraday_diff}
    \nabla \times \mathbf{E} = -\frac{\partial\mathbf{B}}{\partial t}
  \end{equation}

Integralform:
  \begin{equation}
\label{eqn:faraday_integral}
    \oint d\mathbf{s}\cdot \mathbf{E}  = -\frac{\partial\Phi_m}{\partial t}
    = - \frac{\partial}{\partial t} \int d\bm{A}\cdot \bm{B},
  \end{equation}
  wobei der magnetische Flussdichte über die Fläche integriert wird, die das
  geschlossene Wegintegral umschließt. Die Orientierung des Flächenelements
  $d\mathbf{A}$ ergibt sich aus dem Umlaufsinn des Wegintegrals (siehe auch 
  Abschnitt~\ref{subsection:BandBflux}.

 Wir wollen das Faraday'sche Induktionsgesetz anhand von drei 
 Experimenten näher untersuchen (siehe Abb.~\ref{fig:induktion_beispiele}).

 \textbf{Experiment A:} Wir betrachten zwei parallel verlaufende Kupferdrähte, die wir mit einem Widerstand $R$ verbinden. 
 Quer zu den beiden Drähten legen wir 
  einen frei beweglichen Kupferstab. Die ganze Anordnung befindet sich
  in einem homogenen Magnetfeld, 
   so dass die Feldlinien durch die aufgespannte Fläche 
  hindurchgehen. 
  Wir bewegen den Leiter mit der Geschwindigkeit $\bm{v}$
  und messen einen Ausschlag an einem angeschlossenen Voltmeter.
  Der Ausschlag ändert sein Vorzeichen, wenn wir die Bewegungsrichtung ändern. Der Ausschlag wird mit  zunehmender Geschwindigkeit
  größer.

\begin{figure}[h!]
	\parbox{0.49\linewidth}{
	\includegraphics[width=\linewidth]{Graphiken/drawing_schieber_induktor.png}}
    \parbox{0.49\linewidth}{
     \includegraphics[width=\linewidth]{pictures/gerthsen_induktion.png}
    }
\caption{Linkes Bild: Experiment A - wir ändern die Fläche, die von einem Leiter eingefasst wird.
	Rechtes Bild: Experiment B - wir ändern die Magnetfeldstärke, die die Leiterschleife mit
	konstanter Fläche durchdringt.  \label{fig:induktion_beispiele}}
\end{figure}
  \textbf{Experiment B}: Wir nehmen eine Leiterschlaufe, an die wir ein Meßgerät für die Spannung
  anschließen. Wenn wir einen Dipolmagnet so bewegen, dass das magnetische Feld sich zeitlich ändert, 
  messen wir einen Ausschlag. Der Ausschlag hängt von der Geschwindigkeit ab, mit dem wir den Magneten bewegen.\\
  \textbf{Experiment C}: Wenn wir den Magneten in Experiment B fixieren und stattdessen die Leiterschleife so rotieren, dass sich die Ausrichtung von Magnet und Leiterschleife zueinander ändern, kommt es ebenfalls zu einem Ausschlag. Auch hier nimmt der Ausschlag mit der Geschwindigkeit zu und das Vorzeichen ändert sich bei Änderung der Drehrichtung.
\\
Zur Interpretation der Experimente:\\
\textbf{Experiment A:} Fläche ändert sich, Magnetfeld konstant. \\
\textbf{Experiment B:} Fläche ist konstant, Magnetfeld ändert sich.\\
\textbf{Experiment C:} Fläche ist konstant, Magnetfeld ist konstant, aber $d\bm{A}\cdot \bm{B} =
	dA~B \cos\theta$ ändert sich aufgrund der Änderung von $\theta$ (Winkel zwischen Flächennormale
	und Magnetfeld). \\
\textbf{In allen drei Fällen ist $\dot{\phi_m}\ne 0$ und darauf kommt es bei der Induktion an!}

\textbf{Anmerkungen:}
\begin{itemize}	
\item Zur Interpretation von Experiment A: Hier lässt sich auch anhand der Lorentz-Kraft,
die auf die bewegten Ladungen wirkt, argumentieren. Überlegen Sie sich, wie sich eine Spannung 
$V = Bvd$ ausbildet (Hinweis: Vergleichen Sie auch mit dem Hall-Effekt).  
\item Das induzierte elektrische Feld ist im Gegensatz zu dem elektrischen Feld hervorgerufen durch Ladungen (1. Maxwell-Gleichung) nicht-konservativ! Die Feldlinien des induzierten elektrischen
Feldes sind geschlossen (ganz ähnlich zu den Feldlinien des magnetischen Feldes). Das Feld agiert
in ähnlicher Weise wie eine Spannungsquelle, die mit ihrer elektromotorischen Kraft einen Stromkreis 
speist. Deswegen findet sich im Zusammenhang mit dem induzierten elektrischen Feld der Begriff der
elektromotischen Kraft (EMK) in einigen Lehrbüchern.
\end{itemize}
  
 \section{Lenz'sche Regel}
 Die Lenz'sche Regel:\\
    Die durch Veränderung magnetischer Flüsse erzeugten Induktionsströme
    fließen derart, dass ihre eigenen Magnetfelder der Induktionsursache
    entgegenwirken. 

 \begin{figure}
 \includegraphics[width=0.45\linewidth]{pictures/lenz1.png}
 \includegraphics[width=0.45\linewidth]{pictures/lenz2.png}
 \caption{Der induzierete Strom entlang einer Leiterschleife  (in grün) 
 wirkt der Flussänderung entgegen. In den beiden gezeigten Beispielen ist die 
 Richtung des Magnetfelds unverändert. Die zeitliche Änderung des Magnetfeldes 
 bewirkt eine Abnahme des Magnetfeldes (linkes Beispiel) bzw. eine Zunahme des
 Magnetfeldes (rechtes Beispiel). Die induzierten Ströme kompensieren 
 die jeweiligen Änderungen. \label{figure:lenzregel}}
 \end{figure}

 Als Beispiel werden die Änderungen des Magnetfelds in Abbildung 
 \ref{figure:lenzregel} durch die mit den induzierten Strömen erzeugten
 Magnetfelder kompensiert. 
 
 Das negative Vorzeichen in den Gleichungen~\ref{eqn:faraday_diff} und 
 \ref{eqn:faraday_integral}  (und damit die Lenz'sche Regel)
 sorgt dafür, dass es nicht zu einer Instabilität
 kommt: Wenn das Vorzeichen positiv wäre, würde z.B. die zeitliche Zunahme
 eines Magnetfeldes einen Strom induzieren, der diese Magnetfeldzunahme
 weiter verstärkt. Das würde wiederum einen größeren Strom induzieren. 
Eine sich in dieser Weise selbst-verstärkende Reaktion nennen wir auch Instabilität.



\textbf{Demonstrationsexperiment: Lenz'sche Regel mit Ring-Kanone, Gauss-Kanone}

\section{Generator}
\label{section:generator}
Ein zeitlich sich ändernder magnetischer Fluss erzeugt eine induzierte Spannung. Die technische wichtigste Nutzung ist der Generator, bei
dem wir Bewegungsenergie in elektrische Energie umwandeln. Der einfachste Wechselstromgenerator besteht aus einer drehbaren Spule in einem
äußeren Magnetfeld $B$ (z.B. ein Hufeisenmagnet). Wenn sich die Spule  mit $N$ Windungen und der Fläche $A$ im äußeren Magnetfeld dreht, 
wird der magnetische Fluss durch
die rotierende Fläche, die von der Spule aufgespannt wird, periodisch mit der Kreisfrequenz $\omega$ moduliert:
\[ 
 \varphi_m = NBA\cos\omega t.
\]
In der Spule wird dann die Spannung $U_\mathrm{ind}=-\dot\varphi_m$, also
\[
	U_\mathrm{ind} = -NBA\omega \sin(\omega t)
\]
induziert.

   
 \section{Wirbelströme}
  Bislang haben wir die induzierten Ströme in Kabeln betrachtet. Für den Fall, dass wir 
  Ströme in massiven Objekten aus leitendem Material erzeugen, kommt es zur Ausbildung von
  Wirbelströmen, die nach der Lenz'schen Regel der Flussänderung entgegengesetzt sind und 
  dementsprechend der Bewegung des Objekts (oder des Magnetfelds) entgegengesetzt sind. Als
  Beispiel für den Effekt von Wirbelströmen betrachten wir ein Blech aus z.B. Kupfer, das wir
  unter einem Polschuh hindurchbewegen wollen (das könnte z.B. eine Kupferscheibe an einem Rad
  sein, das sich durch einen fest verbauten Magneten hindurchbewegt). In Abbildung~\ref{fig:wirbelbremse} beispielhaft gezeigt, wie in einem inhomogenen Magnetfeld 
  sich Wirbelströme bilden, die ein magnetisches Moment erzeugen. Das magnetische Moment 
  wird durch Abstoßung und Anziehung eine Gesamtkraft erzeugen, die der Bewegungsrichtung 
  entgegengesetzt ist und deren Größe proportional zu der Geschwindigkeit ist.
  
  Die Wirbelströme werden durch Ohm'sche Verluste gedämpft, so dass die kinetische Energie
  in Wärmeenergie in dem Objekt umgewandelt wird. Wirbelstrombremsen haben den Vorteil, kontaktlos
  zu funktionieren und die Wärmeenergie gleichmäßig zu übertragen, so dass auch bei hohen 
  Geschwindigkeiten eine bremsende Wirkung zuverlässig erzeugt wird. Es können jedoch erhebliche
  Kräfte auf den fixierten Permanentmagneten wirken. 
  
  Die Wirbelstrombremse funktioniert auch im umgekehrten Fall, bei dem wir z.B. einen Permanentmagneten durch ein Kupferrohr fallen lassen. Die Wirbelströme im Kupferrohr bremsen
  den Fall.
  
  
  
  \begin{figure}[h!]
  	\begin{center}
  		\includegraphics[width=0.8\linewidth]{Graphiken/drawing_wirbelstrombremse.png}
  	\end{center}
  \caption{\label{fig:wirbelbremse} In einem sich räumlich ändernden Magnetfeld bewegt sich ein 
  (nicht-ferromagnetisches) Objekt aus leitendem Material (z.B. Kupfer, Aluminium). Die
Änderung des magnetischen Feldes wird durch Wirbelströme der Lenz'schen Regel entsprechend 
kompensiert. Das sich bildende magnetische Moment der Wirbelströme wird durch Abstoßung and
Anziehung dazu führen, dass eine Gesamtkraft der Bewegungsrichtung entgegenwirkt. Die
Kraft ist proportional zur Geschwindigkeit und verhält sich also ganz ähnlich zu  Newton'scher
Reibung.}
  \end{figure}
  
 \section{Selbst- und Gegeninduktion}
 \subsection{Selbstinduktion: Induktivität $L$}
 Jede Flussänderung induziert eine Spannung, die nach der Lenz'schen Regel der Flussänderung
 entgegenwirkt. In den bisherigen Beispielen sind entsprechend Ströme in Leiterschleifen induziert 
 worden. 
 
 Für den Fall, dass die Leiterschleife einen sich zeitlich ändernden Strom führt, wirkt die resultierende
 Flussänderung auf die Leiterschleife zurück und induziert eine Spannung, die der Stromrichtung
 entgegenwirkt (siehe auch Abbildung~\ref{fig:selbstinduktion}).
 
 Der magnetische Fluss, der sich durch den Strom ergibt wird in der Regel proportional zum
 Strom sein. Wir setzen also mit einer Proportionalitätskonstane $L$ an:
 \[
 \phi_m = LI,
 \]
 wobei die Einheit für $L$ gegeben ist durch das Verhältnis von magnetischen Fluss zu Strom:
\[
 [L] = \frac{\mathrm{Wb}}{\mathrm{A}}=\frac{\mathrm{Vs}}{A}=\Omega s=\mathrm{H},
\]
wobei das H für Henry steht. 

Für die zeitliche Änderung des Flusses erhalten wir (unter der Annahme, dass die Selbstinduktivität
nicht zeitabhängig ist):
\[
U_\mathrm{ind} = -\dot{\phi_m} = - L\dot{I}.
\]
Die Selbstinduktivität $L$ ergibt sich hierbei hauptsächlich aus der Geometrie des Leiters. 
\begin{example}[Selbst-Induktivität einer Luftspule]
 Für eine Zylinderspule der Länge $\ell$  ohne Eisenjoch ist der 
 magnetische Fluss innerhalb der Spule gegeben
 durch \[ \phi_m = AB =  A \mu_0 n I,  \]
 wobei $n$ die Windungszahl pro Längeneinheit ist. Die induzierte Spannung über die 
 Länge der Spule $\ell$ 
 ergibt sich aus der Anzahl der Windungen $n\ell$:
 \[  U_\mathrm{ind} = -n\ell \dot\phi_m = -n^2 V \mu_0 \dot{I}=-L\dot{I},  \]
 wobei $L= n^2 V \mu_0$ ist (das Volumen $V=\ell A$).   
\end{example}
 
 \begin{figure}[h!]
 	\begin{center}
 	\includegraphics[width=0.8\linewidth]{Graphiken/drawing_selfinduction.png} 	
 \end{center}
\caption{\label{fig:selbstinduktion} Wenn sich der Strom in einem Leiter ändert (hier z.B.
	eine Leiterschlaufe) ergibt sich eine Flussänderung, die einen Strom induziert, der
der Änderung entgegenwirkt. Hier sind zwei Beispiele gegeben: Ansteigender Strom $\dot{I}>0$
erzeugt eine der Stromrichtung entgegenwirkende induzierte Spannung, sinkender Strom ($\dot{I}<0$)
induziert eine Spannung $U_\mathrm{ind}$, die der Änderung entgegenwirkt, also in Stromrichtung zeigt. Die Proportionalitätskonstante für die Selbstinduktion $U_\mathrm{ind}=-L\dot{I}$ wird
\textbf{Selbstinduktivität} genannt. }
 \end{figure}
   
   \subsection{Gegeninduktion}
   TBD.


 \subsection{Anwendung: Transformator}
  Wir betrachten eine Spule (Primärwicklung) mit $N_1$ Windungen auf einem Eisenjoch (z.B. laminiertes Harteisen) - siehe 
  Abbildung~\ref{fig:transformator}. Auf dem selben Eisenjoch 
  ist eine Sekundärwicklung mit $N_2$ Windungen angebracht. Wenn wir jetzt die Primärwicklung mit einem Generator (siehe Abschnitt 
  ~\ref{section:generator}) verbinden, erzeugen wir einen zeitlich variierenden magnetischen Fluss 
  \[ U_1 = - N_1 \dot\varphi.\]
  Der magnetische Fluss $\varphi$ wird im idealen, verlustfreien Transformator durch das Eisenjoch geleitet, so dass die Sekundärspule
  den selben Fluss sieht, so dass dort die Spannung 
  \[ U_2 = - N_2 \dot \varphi \]
  induziert wird.
  Für das Verhältnis der Spannungen im Falle eines unbelasteten Transformators ergibt sich
  \[
	   \frac{U_1}{U_2} = \frac{N_1}{N_2}.
   \]
   Andererseits kann die Leistung $P=U_1 I_1=U_2 I_2$ aufgrund der Energieerhaltung sich nicht ändern, so dass
   \[ \frac{I_2}{I_1} = \frac{N_1}{N_2} \]
   gelten muss. 
   \begin{figure}
	   \begin{center}
	   \includegraphics[width=0.7\linewidth]{figures/transformator.png}
	   \end{center}
	   \caption{\label{fig:transformator} Schematische Darstellung eines Transformators.}

   \end{figure}

   Mit einem Transformator lässt sich die Spannung an der Sekundärwicklung durch ein geeignet gewähltes Verhältnis 
   der Wicklungen entweder hochtransformieren ($N_2>N_1$) oder heruntertransformieren ($N_2<N_1$). Die Ströme in
   der Sekundärspule werden beim Hochtransformieren um das selbe Verhältnis verkleinert. Beim Heruntertransformieren werden die Ströme 
   vergrößert. 

   Im Detail ist beim Transformator zu berücksichtigen, dass ein Teil des magnetischen Flusses nicht in der Sekundärspule ankommt. Ein weiterer
   wichtiger Punkt ist die Phasenlage der beiden Wechselströme, die durch eine Belastung der Sekundärspule modifizert wird. Interessant
   ist auch die Orientierung der Wicklungen zueinander (siehe auch hier die Abbildung~\ref{fig:transformator}). 

  
  
 \section{Maxwell'scher Verschiebungsstrom: Amp\`ere-Maxwell-Gleichung}
 \label{section:maxwell-strom}


Wir haben das Amp\`ere'sche Gesetz $\bm{\nabla}\times \bm{H} = \bm{j}$ (siehe auch Gl.~\ref{eqn:ampereH}) und die Kontinuitätsgleichung \ref{eqn:continuity} kennengelernt. 
Wenn wir jedoch beim Amp\`ere'schen Gesetz auf beiden Seiten die Divergenz bestimmen, stellen wir
fest
\[ \bm{\nabla}\cdot (\bm{\nabla}\times\bm{H})=\bm{\nabla}\cdot\bm{j}, \]
Da $\bm{\nabla}\cdot(\bm{\nabla}\times\bm{H})=0$, müsste auch die Divergenz der Stromdichte
verschwinden - das wäre jedoch nicht konsistent mit der Kontinuitätsgleichung: 
$\bm{\nabla}\cdot \bm{j}=-\dot{\rho}$. Da die Kontinuitätsgleichung eine direkte Konsequenz
der Ladungserhaltung ist, muss das Amp\`ere'sche Gesetz ergänzt werden. Der Ansatz für 
die resultierende Amp\`ere-Maxwell-Gleichung ist ein zusätzlicher Term, der stromartig ist:
\begin{equation}
 \bm{\nabla}\times \bm{H} = \bm{j} + \dot{\bm{D}}.
\end{equation}
Obwohl $\dot{\bm{D}}$ die Einheit Ladung/Zeit hat, wird nicht notwendigerweise  Ladung transportiert. Die Auswirkungen eines zeitlich variierenden elektrischen Feldes sind jedoch ganz
ähnlich wie die einer Stromdichte, d.h. es wird auch dann ein magnetisches Feld erzeugt, wenn
$\bm{j}=0$ und $\dot{\bm{D}}\ne 0$ ist.



