import numpy as np
import matplotlib.pyplot as plt

#%% Plot the fields
X,Y = np.meshgrid( np.arange(-3,3,.25), np.arange(-3,3,.25) )
Ex = (X + 1)/((X+1)**2 + Y**2) - (X - 1)/((X-1)**2 + Y**2)
Ey = Y/((X+1)**2 + Y**2) - Y/((X-1)**2 + Y**2)

plt.figure(figsize=(4.5, 4.5),facecolor="w")

plt.plot([-1],[0],'ro',ms=14)
plt.plot([+1],[0],'go',ms=14)
#plt.streamplot(X,Y,Ex,Ey)
plt.quiver(X,Y,Ex,Ey,scale=50)
#plt.title('streamplot')
plt.savefig('electric_vector_1.png',dpi=250,bbox_inches="tight",pad_inches=0.02)
plt.show()
plt.show()
